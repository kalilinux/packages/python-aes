from setuptools import find_packages, setup

setup(
    name = "pythonaes",
    version = "1.0",
    author = "Demur Rumed",
    url = "https://github.com/serprex/pythonaes",
    packages = find_packages(),
)
